from dotmap import DotMap
from aiohttp import web
import json
from bson import ObjectId
import logging

logger = logging.getLogger("tester")

class HandlerComponent:
    def __init__(self, app):
        self.db = app['config']['db']
        self.validator = app['config']['validator']

        self.dbNameCompon = 'component'
        self.dbNameProject = 'project'

    async def componentList(self, request):
        try:
            projectName = request.match_info.get('projectName', "")

            if not await self.db.checkElementInCollection(self.dbNameProject, 'name', projectName):
                return web.StreamResponse(status=404, reason='Not Fount Project') 
            
            project = await self.db.getElementInCollection(self.dbNameProject, 'name', projectName)

            projects = await self.db.getLongElementsInCollection(self.dbNameCompon, filterDb={'_idProject': project['_id']})
            if projects != False:
                for project in projects:
                    project['projectName'] = projectName
                return web.json_response(projects)
            else:
                 raise ValueError('Error in get Long Elements in collection...')

        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')   

    async def componentCreate(self, request):
        try:
            projectName = request.match_info.get('projectName', "")
            if not await self.db.checkElementInCollection(self.dbNameProject, 'name', projectName):
                return web.StreamResponse(status=404, reason='Not Fount Project') 

            data = await request.json()

            project = await self.db.getElementInCollection(self.dbNameProject, 'name', projectName)

            data['_idProject'] = project['_id']

            checkData = self.validator.checkRequest('componentCreate', data)
            if not checkData['state']:
                return web.StreamResponse(status=400, reason='Bad Request')  
            
            if await self.db.checkElementInCollection(
                self.dbNameCompon, 
                'name', data.get('name'),
                filterDb={"_idProject": data.get('_idProject')}
            ):
                return web.StreamResponse(status=409, reason='Conflict') 

            if await self.db.addElementInCollection(self.dbNameCompon, data):
                return web.StreamResponse(status=200, reason='OK')

        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')  

    async def componentDelete(self, request):
        try:
            projectName = request.match_info.get('projectName', "")
            componentName = request.match_info.get('componentName', "")

            if not await self.db.checkElementInCollection(self.dbNameProject, 'name', projectName):
                return web.StreamResponse(status=404, reason='Not Fount Project') 

            project = await self.db.getElementInCollection(self.dbNameProject, 'name', projectName)
            component = await self.db.getElementInCollection(
                self.dbNameCompon, 
                'name', componentName,
                filterDb={'_idProject': project.get('_id')}
                )

            if not component:
                return web.StreamResponse(status=404, reason='Not Fount Component') 

            if await self.db.deleteElementsInCollection(
                self.dbNameCompon, 
                'name', componentName,
                filterDb={'_idProject': project.get('_id')}
                ):
                return web.StreamResponse(status=200, reason='OK')     
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')   

    async def componentDeleteOther(self, request):
        try:
            projectName = request.match_info.get('projectName', "")

            data = await request.json()
            checkData = self.validator.checkRequest('componentDeleteOther', data)

            if not checkData['state']:
                return web.StreamResponse(status=400, reason='Bad Request')   

            if not await self.db.checkElementInCollection(self.dbNameProject, 'name', projectName):
                return web.StreamResponse(status=404, reason='Not Fount Project') 

            if not await self.db.checkElementInCollection(self.dbNameCompon, list(data.keys())[0], list(data.values())[0]) and not data:
                return web.StreamResponse(status=404, reason='Not Fount Component')

            if await self.db.deleteElementsInCollection(self.dbNameCompon, list(data.keys())[0], list(data.values())[0]):
                return web.StreamResponse(status=200, reason='OK')
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')          

    async def componentSet(self, request):
        try:
            projectName = request.match_info.get('projectName', "")
            componentName = request.match_info.get('componentName', "")

            if not await self.db.checkElementInCollection(self.dbNameProject, 'name', projectName):
                return web.StreamResponse(status=404, reason='Not Fount Project') 

            project = await self.db.getElementInCollection(self.dbNameProject, 'name', projectName)

            if not await self.db.checkElementInCollection(
                self.dbNameCompon, 
                'name', componentName,
                filterDb={'_idProject': project.get('_id')}
                ):
                return web.StreamResponse(status=404, reason='Not Fount Component') 

            data = await request.json()

            checkData = self.validator.checkRequest('componentSet', data)
            if not checkData['state']:
                return web.StreamResponse(status=400, reason='Bad Request')  

            if data.get('projectName', False):
                # Нельзя менять проект у компонента
                return web.StreamResponse(status=400, reason='Bad Request')  

            if data.get('name', False):
                if await self.db.checkElementInCollection(
                    self.dbNameCompon, 
                    'name', data.get('name'),
                    filterDb={"_idProject": data.get('_idProject')}
                    ):
                    return web.StreamResponse(status=409, reason='Conflict') 

            if await self.db.updateElementsInCollection(
                self.dbNameCompon, 
                'name', componentName, 
                data,
                filterDb={'_idProject': project.get('_id')}
                ):
                return web.StreamResponse(status=200, reason='OK')  

        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')  
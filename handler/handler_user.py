from dotmap import DotMap
from aiohttp import web
import json
import sys
import motor.motor_asyncio
from aiohttp_session import SimpleCookieStorage, session_middleware
from aiohttp_security import check_permission, \
    is_anonymous, remember, forget, \
    setup as setup_security, SessionIdentityPolicy
from aiohttp_security.abc import AbstractAuthorizationPolicy
from passlib.hash import pbkdf2_sha256

sys.path.append('..')
from modules.authPolicy import check_credentials
import logging

logger = logging.getLogger("tester")


class HandlerUser:
    def __init__(self, app):
        self.db = app['config']['db']
        self.validator = app['config']['validator']
        self.users_collection = 'users'
        self.permissions_collection = 'permissions'

    async def userLogin(self, request):
        redirect_response = web.Response(status=200, reason='OK')
        try:
            data = await request.json()
            # TODO: валидация авторизации
            if await check_credentials(self.db, data.get('login'), data.get('password')):
                await remember(request, redirect_response, data.get('login'))
                return redirect_response   
            else:
                return web.StreamResponse(status=404, reason='User if not fount.')   
        
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors') 

    async def userLogout(self, request):
        redirect_response = web.Response(status=200, reason='OK')
        await forget(request, redirect_response)
        return redirect_response  
     
    async def userCreate(self, request):
        try:
            redirect_response = web.HTTPFound('/')
            data = await request.json()
            checkData = self.validator.checkRequest('userCreate', data)
            if not checkData['state']:
                return web.StreamResponse(status=400, reason='Bad Request')

            login = data.get('login')
            password = data.get('password')
            is_superuser = data.get('is_superuser', False)
            permits = data.get('permits', [])

            if not await self.db.checkElementInCollection(self.users_collection, 'login', login):
                if await self.db.addElementInCollection(
                    self.users_collection,
                    {
                        "login": login,
                        "passwd": pbkdf2_sha256.hash(password),
                        "is_superuser": is_superuser,
                        "disabled": False
                    }
                    ):
                    for permit in permits:
                        user = await self.db.getElementInCollection(self.users_collection, 'login', login)
                        if user:
                            if await self.db.addElementInCollection(
                                self.permissions_collection,
                                {
                                    "user_id": user.get("_id"),
                                    "perm_name": permit
                                }):
                                logger.debug(f"Add permit user '{login}'...")
                            else:
                                logger.debug(f"Cant ser permit user '{login}'...")
                    return web.StreamResponse(status=200, reason='OK')

                else:
                    raise ValueError('Error in create user...')

                await remember(request, redirect_response, login)
                raise redirect_response
            else:
                return web.StreamResponse(status=409, reason='Conflict') 
            
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')   

    async def userSet(self, request):
        try:
            userName = request.match_info.get('userName', "")
            data = await request.json()
            
            checkData = self.validator.checkRequest('userSet', data)
            if not checkData['state']:
                return web.StreamResponse(status=400, reason='Bad Request')


            permits = False

            if data.get('permits', False):
                permits = data['permits']
                del data['permits']

            if data.get('password', False):
                data['passwd'] = pbkdf2_sha256.hash(data['password'])

            if await self.db.checkElementInCollection(self.users_collection, 'login', userName):
                user = await self.db.getElementInCollection(self.users_collection, 'login', userName)

                if data.get('login', False):
                    if await self.db.checkElementInCollection(self.users_collection, 'login', data.get('login')):
                        return web.StreamResponse(status=409, reason='Conflict') 

                if not await self.db.updateElementsInCollection(
                    self.users_collection, 
                    '_id', 
                    user.get('_id'),
                    data):
                    raise ValueError("Error update element...")

                if permits:
                    if await self.db.deleteElementsInCollection(
                        self.permissions_collection, 
                        'user_id', 
                        user.get('_id', '')):
                            for permit in permits:
                                await self.db.addElementInCollection(
                                    self.permissions_collection,
                                    {
                                        "user_id": user.get("_id"),
                                        "perm_name": permit
                                    })
            else:
                return web.StreamResponse(status=404, reason='Not Fount User') 
            
            return web.StreamResponse(status=200, reason='OK')
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')    

    async def userList(self, request):
        try:
            usersList = await self.db.getLongElementsInCollection(self.users_collection)
            if usersList != False:
                for user in usersList:
                    index = usersList.index(user)
                    del user['passwd']
                    usersList[index] = user
                return web.json_response(usersList)
            else:
                raise ValueError('Error in get Long Elements in collection...')
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')  

    async def userInfo(self, request):
        try:
            userName = request.match_info.get('userName', "")
            if await self.db.checkElementInCollection(self.users_collection, 'login', userName):
                user = await self.db.getElementInCollection(self.users_collection, 'login', userName)
                del user['passwd']
                usersPermits = await self.db.getLongElementsInCollection(
                    self.permissions_collection, 
                    filterDb={'user_id': user.get('_id')}
                    )

                user['permits'] = []
                for permit in usersPermits:
                    user['permits'].append(permit.get('perm_name', ''))
                return web.json_response(user)
            else:
                return web.StreamResponse(status=404, reason='Not Fount User') 
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')  

    async def userDelete(self, request):
        try:
            userName = request.match_info.get('userName', "")
            if await self.db.checkElementInCollection(self.users_collection, 'login', userName):
                user = await self.db.getElementInCollection(self.users_collection, 'login', userName)
                if user:
                    if await self.db.deleteElementsInCollection(self.users_collection, 'login', userName):
                            if await self.db.deleteElementsInCollection(self.permissions_collection, 'user_id', user.get('_id', '')):
                                return web.StreamResponse(status=200, reason='OK')
            raise ValueError(f"Errors delete user {userName}")   
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')   

        
from dotmap import DotMap
from aiohttp import web
import json
from bson import ObjectId
import datetime
import logging

logger = logging.getLogger("tester")

class HandlerTest:
    def __init__(self, app):
        self.db = app['config']['db']
        self.validator = app['config']['validator']

        self.dbNameTest = 'test'
        self.dbNameCompon = 'component'
        self.dbNameProject = 'project'
        self.dbNameProcedure = 'procedure'
                                                                      
    async def testList(self, request):
        try:
            projectName = request.match_info.get('projectName', "")

            project = await self.db.getElementInCollection(self.dbNameProject, 'name', projectName)

            if project:
                tests = await self.db.getShortElementsInCollection(
                    self.dbNameTest, 
                    filterDb={'_idProject': project['_id']}
                    )
            else:
                tests = await self.db.getShortElementsInCollection(
                    self.dbNameTest
                    )
            
            return web.json_response(tests)
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')

    async def testCreate(self, request):
        try:
            projectName = request.match_info.get('projectName', "")

            data = await request.json()
            checkData = self.validator.checkRequest('testCreate', data)
            if not checkData['state']:
                return web.StreamResponse(status=400, reason='Bad Request')

            project = await self.db.getElementInCollection(self.dbNameProject, 'name', projectName)
 
            if not project:
                return web.StreamResponse(status=404, reason='Not Fount Project') 

            if await self.db.checkElementInCollection(
                self.dbNameTest, 
                'name', data['name'], 
                filterDb={'_idProject': project['_id']
                }):
                return web.StreamResponse(status=409, reason='Conflict')

            procedures = await self.db.getShortElementsInCollection(self.dbNameProcedure, filterDb={'_idProject': project['_id']})
            procedures = [proc['name'] for proc in procedures]

            for proc in data['testProcedure']:
                if not proc['name'] in procedures:
                    return web.StreamResponse(status=400, reason='Bad Request')  

            data['_idProject'] = project['_id']

            if await self.db.addElementInCollection(self.dbNameTest, data):
                return web.StreamResponse(status=200, reason='OK')
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')

    async def testInfo(self, request):
        try:
            testName = request.match_info.get('testName', "")
            projectName = request.match_info.get('projectName', "")

            project = await self.db.getElementInCollection(self.dbNameProject, 'name', projectName)

            if not project:
                return web.StreamResponse(status=404, reason='Not Fount Project') 

            if not await self.db.checkElementInCollection(
                self.dbNameTest, 
                'name', testName,
                filterDb={'_idProject': project['_id']}
                ):
                return web.StreamResponse(status=404, reason='Not Fount test') 
            
            data = await self.db.getElementInCollection(
                self.dbNameTest, 
                'name', testName,
                filterDb={'_idProject': project['_id']}
                )
            
            return web.json_response(data)
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')

    async def testDelete(self, request):
        try:
            testName = request.match_info.get('testName', "")
            projectName = request.match_info.get('projectName', "")

            project = await self.db.getElementInCollection(self.dbNameProject, 'name', projectName)

            if not project:
                return web.StreamResponse(status=404, reason='Not Fount Project') 

            if not await self.db.checkElementInCollection(
                self.dbNameTest, 
                'name', testName,
                filterDb={'_idProject': project['_id']}
                ):
                return web.StreamResponse(status=404, reason='Not Fount Test') 

            if await self.db.deleteElementsInCollection(
                self.dbNameTest, 
                'name', testName,
                filterDb={'_idProject': project['_id']}
                ):
                return web.StreamResponse(status=200, reason='OK')     
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors') 


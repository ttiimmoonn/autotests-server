from dotmap import DotMap
from aiohttp import web
import json
from bson import ObjectId
import datetime
import logging

logger = logging.getLogger("tester")

class HandlerVersion:
    def __init__(self, app):
        self.db = app['config']['db']
        self.validator = app['config']['validator']

        self.dbNameCompon = 'component'
        self.dbNameProject = 'project'
        self.dbNameVersion = 'version'


    async def checkProjectAndComponent(self, projectName, componentName):
        if not await self.db.checkElementInCollection(self.dbNameProject, 'name', projectName):
            return {'status': False, 'reason': 'Not Fount Project'}

        if not await self.db.checkElementInCollection(
            self.dbNameCompon, 
            'name', 
            componentName,
            filterDb= {'projectName': projectName}):
            return {'status': False, 'reason': 'Not Fount Component'}
        
        return {'status': True}

    async def versionList(self, request):
        try:
            projectName = request.match_info.get('projectName', "")
            componentName = request.match_info.get('componentName', "")

            chk = await self.checkProjectAndComponent(projectName, componentName)
            if not chk['status']:
                return web.StreamResponse(status=404, reason=chk['reason']) 

            versions = await self.db.getLongElementsInCollection(
                self.dbNameVersion, 
                filterDb= {
                    'projectName': projectName,
                    'componentName': componentName
                    })
            if versions != False:
                for vers in versions:
                    if vers.get('last_modified', False):
                        index = versions.index(vers)
                        versions[index]['last_modified'] = versions[index]['last_modified'].isoformat(sep='T')
                return web.json_response(versions)
            else:
                 raise ValueError('Error in get Long Elements in collection...')
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')

    async def versionSet(self, request):
        try:
            projectName = request.match_info.get('projectName', "")
            componentName = request.match_info.get('componentName', "")
            versionName = request.match_info.get('versionName', "")

            chk = await self.checkProjectAndComponent(projectName, componentName)
            if not chk['status']:
                return web.StreamResponse(status=404, reason=chk['reason']) 

            if not await self.db.checkElementInCollection(self.dbNameVersion, 'name', versionName):
                return web.StreamResponse(status=404, reason='Not Fount Version')

            data = await request.json()
            data['last_modified'] = datetime.datetime.now()

            checkData = self.validator.checkRequest('versionSet', data)
            if not checkData['state']:
                return web.StreamResponse(status=400, reason='Bad Request') 

            if data.get('projectName', False):
                if not await self.db.checkElementInCollection(self.dbNameProject, 'name', data['projectName']):
                    return web.StreamResponse(status=404, reason='Not Fount Project') 

            if data.get('componentName', False):
                if not await self.db.checkElementInCollection(
                    self.dbNameCompon, 
                    'name', 
                    data['componentName'],
                    filterDb= {'projectName': projectName}):
                    return web.StreamResponse(status=404, reason='Not Fount Component')
            
            if data.get('name', False):
                if await self.db.checkElementInCollection(
                    self.dbNameVersion, 
                    'name', 
                    data['name'], 
                    filterDb= {
                        'projectName': projectName,
                        'componentName': componentName
                        }):
                    return web.StreamResponse(status=409, reason='Conflict') 

            if await self.db.updateElementsInCollection(self.dbNameVersion, 'name', versionName, data):
                return web.StreamResponse(status=200, reason='OK')  
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')  

    async def versionCreate(self, request):
        try:
            projectName = request.match_info.get('projectName', "")
            componentName = request.match_info.get('componentName', "")

            chk = await self.checkProjectAndComponent(projectName, componentName)
            if not chk['status']:
                return web.StreamResponse(status=404, reason=chk['reason']) 

            data = await request.json()
            data['projectName'] = projectName
            data['componentName'] = componentName
            data['last_modified'] = datetime.datetime.now()

            checkData = self.validator.checkRequest('versionCreate', data)
            if not checkData['state']:
                return web.StreamResponse(status=400, reason='Bad Request')  

            if await self.db.checkElementInCollection(self.dbNameVersion, 'name', data.get('name', '')):
                return web.StreamResponse(status=409, reason='Conflict') 

            if await self.db.addElementInCollection(self.dbNameVersion, data):
                return web.StreamResponse(status=200, reason='OK')

        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')  

    async def versionDelete(self, request):
        try:
            projectName = request.match_info.get('projectName', "")
            componentName = request.match_info.get('componentName', "")
            versionName = request.match_info.get('versionName', "")

            chk = await self.checkProjectAndComponent(projectName, componentName)
            if not chk['status']:
                return web.StreamResponse(status=404, reason=chk['reason'])
            
            if not await self.db.checkElementInCollection(self.dbNameVersion, 'name', versionName):
                return web.StreamResponse(status=404, reason='Not Fount Version') 

            if await self.db.deleteElementsInCollection(self.dbNameVersion, 'name', versionName):
                return web.StreamResponse(status=200, reason='OK') 

        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')

    async def versionDeleteOther(self, request):
        pass
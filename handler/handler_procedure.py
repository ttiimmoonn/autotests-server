from dotmap import DotMap
from aiohttp import web
import json
from bson import ObjectId
import datetime
import logging

logger = logging.getLogger("tester")

class HandlerProcedure:
    def __init__(self, app):
        self.db = app['config']['db']
        self.validator = app['config']['validator']

        self.dbNameCompon = 'component'
        self.dbNameProject = 'project'
        self.dbNameProcedure = 'procedure'
                                                                      
    async def procedureList(self, request):
        try:
            projectName = request.match_info.get('projectName', "")
            

            if projectName:
                project = await self.db.getElementInCollection(self.dbNameProject, 'name', projectName)

                if not project:
                    return web.StreamResponse(status=404, reason='Not Fount Project') 

                procedure = await self.db.getShortElementsInCollection(
                    self.dbNameProcedure, 
                    filterDb={'_idProject': project['_id']}
                    )
            else:
                procedure = await self.db.getShortElementsInCollection(self.dbNameProcedure)
            
            return web.json_response(procedure)
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')

    async def procedureCreate(self, request):
        try:
            projectName = request.match_info.get('projectName', "")

            project = await self.db.getElementInCollection(self.dbNameProject, 'name', projectName)

            if not project:
                return web.StreamResponse(status=404, reason='Not Fount Project') 

            data = await request.json()

            checkData = self.validator.checkRequest('procedureCreate', data)
            if not checkData['state']:
                return web.StreamResponse(status=400, reason='Bad Request')  

            if await self.db.checkElementInCollection(
                self.dbNameProcedure, 
                'name', data.get('name'), 
                filterDb={'_idProject': project['_id']}
                ):
                return web.StreamResponse(status=409, reason='Conflict')
            
            data['_idProject'] = project['_id']

            if await self.db.addElementInCollection(self.dbNameProcedure, data):
                return web.StreamResponse(status=200, reason='OK')
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')

    async def procedureInfo(self, request):
        try:
            procedureName = request.match_info.get('procedureName', "")
            projectName = request.match_info.get('projectName', "")

            project = await self.db.getElementInCollection(self.dbNameProject, 'name', projectName)

            if not project:
                return web.StreamResponse(status=404, reason='Not Fount Project') 
            
            data = await self.db.getElementInCollection(
                self.dbNameProcedure, 
                'name', procedureName,
                filterDb={'_idProject': project['_id']}
                )
            
            return web.json_response(data)
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')

    async def procedureDelete(self, request):
        try:
            procedureName = request.match_info.get('procedureName', "")
            projectName = request.match_info.get('projectName', "")

            project = await self.db.getElementInCollection(self.dbNameProject, 'name', projectName)

            if not project:
                return web.StreamResponse(status=404, reason='Not Fount Procedure') 

            if await self.db.deleteElementsInCollection(
                self.dbNameProcedure, 
                'name', procedureName,
                filterDb={'_idProject': project['_id']}
                ):
                return web.StreamResponse(status=200, reason='OK')     
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors') 


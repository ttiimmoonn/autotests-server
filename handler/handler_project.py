from dotmap import DotMap
from aiohttp import web
import json
import motor.motor_asyncio
from bson import ObjectId
import logging

logger = logging.getLogger("tester")

class HandlerProject:
    def __init__(self, app):
        self.db = app['config']['db']
        self.validator = app['config']['validator']

        self.dbNameProject = 'project'
        self.dbNameCompon = 'component'        
        self.dbNameTest = 'test'
        self.dbNameProcedure = 'procedure'

    async def projectList(self, request):
        try:  
            projects = await self.db.getLongElementsInCollection(self.dbNameProject)
            if projects != False:
                return web.json_response(projects)
            else:
                raise ValueError('Error in get Long Elements in collection...')
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')     

    async def projectCreate(self, request):
        try:
            data = await request.json()
            checkData = self.validator.checkRequest('projectCreate', data)
            if not checkData['state']:
                return web.StreamResponse(status=400, reason='Bad Request')   

            if await self.db.checkElementInCollection(self.dbNameProject, 'name', data.get('name', '')):
                return web.StreamResponse(status=409, reason='Conflict') 

            if await self.db.addElementInCollection(self.dbNameProject, data):
                return web.StreamResponse(status=200, reason='OK')
        except Exception as e:
            return web.StreamResponse(status=500, reason='Server errors')     

    async def projectSet(self, request):
        try:
            projectName = request.match_info.get('projectName', "")
            data = await request.json()
            checkData = self.validator.checkRequest('projectSet', data)
            if not checkData['state']:
                return web.StreamResponse(status=400, reason='Bad Request')   

            if not await self.db.checkElementInCollection(self.dbNameProject, 'name', projectName):
                return web.StreamResponse(status=404, reason='Not Fount Project') 

            if await self.db.updateElementsInCollection(self.dbNameProject, 'name', projectName, data):
                return web.StreamResponse(status=200, reason='OK')     
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')        

    async def projectInfo(self, request):
        try:
            projectName = request.match_info.get('projectName', "")

            if not await self.db.checkElementInCollection(self.dbNameProject, 'name', projectName):
                return web.StreamResponse(status=404, reason='Not Fount Project') 

            project = await self.db.getElementInCollection(self.dbNameProject, 'name', projectName)
            return web.json_response(project)

        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')   

    async def projectDelete(self, request):
        try:
            projectName = request.match_info.get('projectName', "")
            
            project = await self.db.getElementInCollection(self.dbNameProject, 'name', projectName)

            if not project:
                return web.StreamResponse(status=404, reason='Not Fount Project') 

            await self.db.deleteElementsInCollection(self.dbNameProject, filterDb={'_id': project['_id']})
            await self.db.deleteElementsInCollection(self.dbNameCompon, filterDb={'_idProject': project['_id']})
            await self.db.deleteElementsInCollection(self.dbNameTest, filterDb={'_idProject': project['_id']})
            await self.db.deleteElementsInCollection(self.dbNameProcedure, filterDb={'_idProject': project['_id']})

            return web.StreamResponse(status=200, reason='OK') 

        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')    

    async def projectDeleteOther(self, request):
        try:
            data = await request.json()
            checkData = self.validator.checkRequest('projectDeleteOther', data)

            if not checkData['state']:
                return web.StreamResponse(status=400, reason='Bad Request')   

            projects = await self.db.getLongElementsInCollection(self.dbNameProject, filterDb=data)

            if not projects:
                return web.StreamResponse(status=404, reason='Not Fount Project') 

            for project in projects:
                await self.db.deleteElementsInCollection(self.dbNameProject, filterDb={'_id': project['_id']})
                await self.db.deleteElementsInCollection(self.dbNameCompon, filterDb={'_idProject': project['_id']})
                await self.db.deleteElementsInCollection(self.dbNameTest, filterDb={'_idProject': project['_id']})
                await self.db.deleteElementsInCollection(self.dbNameProcedure, filterDb={'_idProject': project['_id']})

            return web.StreamResponse(status=200, reason='OK')    
        except Exception as e:
            logger.error(f'{e}')
            return web.StreamResponse(status=500, reason='Server errors')     
FROM python:3.8-buster

COPY . /autotest-server
WORKDIR /autotest-server

RUN ["python3.8", "-m", "pip", "install", "-r", "requirements.txt"]


VOLUME ["/autotest-server/config", "/autotest-server/log"]
EXPOSE 8082

CMD ["python3.8", "./main.py"]
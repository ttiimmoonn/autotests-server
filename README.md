Сервер для управления тестами.

# Autotests-server
Сервер для управления тестами.

### Dependencies:


#### Python interpreter:

<ul><li>Python 3.8 or greater</li></ul>

#### Python libraries:
Согласно файлу requirements.


#### Configure:
Настройки хранятся по пути config/conf.json:
```json
{
    "host": "0.0.0.0",        - адрес для bind сервера
    "port": "8082",           - порт на котором слушает сервер
    "db": {
      "type": "mongodb",      - база данных используемая для хранения тестов
      "host": "192.168.1.34", - ip адрес базы данных
      "port": 27017           - port пор базы данных
    },
    "log": {
      "level": "DEBUG"        - уровень логов
    }
}
```

#### Docker:
Для закуска необходимо:
* проброс порта 8082
* директории с конфигурацией "/python/autotests-server/config:/autotest-server/config"
* директория с логами "/autotest-server/log"

Пример запуска:
```sudo docker run -p 8082:8082 -v /python/autotests-server/config:/autotest-server/config autotests-server/dev```




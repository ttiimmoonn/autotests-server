import pathlib
import json
import logging
import os

BASE_DIR = pathlib.Path(__file__).parent

# Путь до конфигураций сервера
config_path = BASE_DIR / 'config' / 'conf.json'
# Путь до файлов с логами
log_path = BASE_DIR / 'log'


def create_logger(path, level='INFO'):
    logger = logging.getLogger('tester')
    handler = logging.StreamHandler()

    if level == 'DEBUG':
        logger.setLevel(logging.DEBUG)

        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter("%(asctime)-8s %(levelname)-8s [%(module)s:%(lineno)d] %(message)-8s")
        handler.setFormatter(formatter)
        logger.addHandler(handler)

        handler = logging.FileHandler("{}/apiDebug.log".format(path), "w")
        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter("%(asctime)-8s %(levelname)-8s [%(module)s:%(lineno)d] %(message)-8s")
        handler.setFormatter(formatter)
        logger.addHandler(handler)

    elif level == 'ERROR':
        logger.setLevel(logging.ERROR)

        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter("%(asctime)-8s %(levelname)-8s [%(module)s:%(lineno)d] %(message)-8s")
        handler.setFormatter(formatter)
        logger.addHandler(handler)

    else:
        logger.setLevel(logging.INFO)

        handler.setLevel(logging.INFO)
        formatter = logging.Formatter("%(asctime)-8s %(levelname)-8s [%(module)s:%(lineno)d] %(message)-8s")
        handler.setFormatter(formatter)
        logger.addHandler(handler)


    handler = logging.FileHandler("{}/apiError.log".format(path), "w", encoding=None, delay="true")
    handler.setLevel(logging.ERROR)
    formatter = logging.Formatter("%(asctime)-8s %(levelname)-8s [%(module)s:%(lineno)d] %(message)-8s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    return logger


def get_config(path):
    with open(path) as f:
        config = json.load(f)
    return config

if __name__ != '__main__':
    if os.path.isfile(config_path):
        config = get_config(config_path)
        if config.get("log", False):
            log_dir = config['log'].get('path', log_path)
            if not os.path.exists(log_path):
                os.mkdir(log_dir)

            logger = create_logger(
                log_dir,
                config['log'].get('level', 'INFO')
                )
        else:
            logger = create_logger(log_path)
    else:
        print('Config file not found! Expected file : "<work_dir>/config/conf.json"')
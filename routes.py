import aiohttp_cors
from handler.handler_component import HandlerComponent
from handler.handler_version import HandlerVersion
from handler.handler_project import HandlerProject
from handler.handler_user import HandlerUser
from handler.handler_procedure import HandlerProcedure
from handler.handler_test import HandlerTest




# Формируем api сервера 
def setup_routes(app, logger):
    # Формируем класс обработчиков
    logger.debug('We form a class handler...')
    try:
        handlerComponent = HandlerComponent(app)
        handlerVersion = HandlerVersion(app)
        handlerProject = HandlerProject(app)
        handlerUser = HandlerUser(app)
        handlerProcedure = HandlerProcedure(app)
        handlerTest = HandlerTest(app)

    except Exception as e:
        logger.error('Error form a class handler.')
        logger.error(e)
    routes = [
        # Работа с пользователем
        # Создание пользователя
        ('POST', '/api/v1/users/create', handlerUser.userCreate, 'user_create'),

        # Авторизация пользователя
        ('POST', '/api/v1/users/login', handlerUser.userLogin, 'user_login'),

        # Выход пользователя
        ('GET', '/api/v1/users/logout', handlerUser.userLogout, 'user_logout'),
        
        # Создание пользователя
        ('POST', '/api/v1/users/registration', handlerUser.userCreate, 'user_registration'),
        
        # Информация о пользователе
        ('GET', '/api/v1/users/{userName}/info', handlerUser.userInfo, 'user_info'),

        # Все пользователи 
        ('GET', '/api/v1/users/list', handlerUser.userList, 'user_list'),

        # Удалить пользователя 
        ('DELETE', '/api/v1/users/{userName}/delete', handlerUser.userDelete, 'user_delete'),

        # Обновить пользователя
        ('POST', '/api/v1/users/{userName}/set', handlerUser.userSet, 'user_set'),


        # Список проектов c описанием и компонентами
        ('GET', '/api/v1/project/list', handlerProject.projectList, 'show_project_list'),

        # Создать проект
        ('POST', '/api/v1/project/create', handlerProject.projectCreate, 'create_project'),

        # Поменять параметры проекта
        ('POST', '/api/v1/project/{projectName}/set', handlerProject.projectSet, 'set_props_project'),

        # Удалить проект
        ('DELETE', '/api/v1/project/{projectName}/delete', handlerProject.projectDelete, 'delete_project'),

        # Удалить проект по кос. признаку
        ('POST', '/api/v1/project/delete', handlerProject.projectDeleteOther, 'delete_project_other'),

        # Информация о  проекте
        ('GET', '/api/v1/project/{projectName}/info', handlerProject.projectInfo, 'info_project'),

        

        # Список список всех компонентов с описанием и версиями
        ('GET', '/api/v1/component/{projectName}/list', handlerComponent.componentList, 'show_component'),

        # Добавить компонент в базу
        ('POST', '/api/v1/component/{projectName}/create', handlerComponent.componentCreate, 'create_component'),
        
        # Список список всех компонентов с описанием и версиями
        ('POST', '/api/v1/component/{projectName}/{componentName}/set', handlerComponent.componentSet, 'set_props_component'),
                
        # Список список всех компонентов с описанием и версиями
        ('DELETE', '/api/v1/component/{projectName}/{componentName}/delete', handlerComponent.componentDelete, 'delete_component'),
        
        # Список список всех компонентов с описанием и версиями
        ('POST', '/api/v1/component/{projectName}/delete', handlerComponent.componentDeleteOther, 'delete_component_other'),
        

        # Создать версию с описанием
        ('POST', '/api/v1/version/{projectName}/{componentName}/create', handlerVersion.versionCreate, 'create_version'),

        # Список список всех компонентов с описанием и версиями
        ('DELETE', '/api/v1/version/{projectName}/{componentName}/{versionName}/delete', handlerVersion.versionDelete, 'delete_version'),

        # Изменить парметры версии
        ('POST', '/api/v1/version/{projectName}/{componentName}/{versionName}/set', handlerVersion.versionSet, 'set_props_version'),

        # Список список всех компонентов с описанием и версиями
        ('GET', '/api/v1/version/{projectName}/{componentName}/list', handlerVersion.versionList, 'show_version'),


        # Список всех процедур проекта
        ('GET', '/api/v1/procedure/{projectName}/list', handlerProcedure.procedureList, 'show_procedure'),

        # Список всех процедур
        ('GET', '/api/v1/procedure/list', handlerProcedure.procedureList, 'show_all_procedure'),

        # Создать тестовую процедуру
        ('POST', '/api/v1/procedure/{projectName}/create', handlerProcedure.procedureCreate, 'create_procedure'),

        # Информация о тестовой процедуре
        ('GET', '/api/v1/procedure/{projectName}/{procedureName}/info', handlerProcedure.procedureInfo, 'info_procedure'),

        # Удаление тестовой процедуры
        ('DELETE', '/api/v1/procedure/{projectName}/{procedureName}/delete', handlerProcedure.procedureDelete, 'delete_procedure'),


        # Список всех тестов проекта
        ('GET', '/api/v1/test/{projectName}/list', handlerTest.testList, 'show_test'),

        # Список всех тестов
        ('GET', '/api/v1/test/list', handlerTest.testList, 'show_all_test'),

        # Создать тест
        ('POST', '/api/v1/test/{projectName}/create', handlerTest.testCreate, 'create_test'),

        # Информация о тесте
        ('GET', '/api/v1/test/{projectName}/{testName}/info', handlerTest.testInfo, 'info_test'),

        # Удаление тестовой процедуры
        ('DELETE', '/api/v1/test/{projectName}/{testName}/delete', handlerTest.testDelete, 'delete_test')

        ]

    '''
    # Добавляем маршруты с обработчиками в приложение
    logger.debug('Add routes with handlers to the application...')
    for route in routes:
        try:
            app.router.add_route(route[0], route[1], route[2], name=route[3])
        except Exception as e:
            logger.error('Error add route: name - {}.'.format(route[3]))
            logger.error(e)
    '''

    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*"
        ) 
    })

    logger.debug('Add routes with handlers to the application...')
    for route in routes:
        try:
            cors.add(app.router.add_route(route[0], route[1], route[2], name=route[3]))
        except Exception as e:
            logger.error('Error add route: name - {}.'.format(route[3]))
            logger.error(e)
'''
        
        # Инфомация о тесте
        ('GET', '/api/tests/{testid}/info', handler.testInfo, 'show_test_info'),
        
        # Инфомация о тесте
        ('GET', '/api/tests/{testid}/execution_history', handler.testExecutionHistory, 'show_execution_history')
'''
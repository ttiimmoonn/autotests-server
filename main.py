#!/usr/bin/python3.8

from aiohttp import web
from aiohttp_session_mongo import MongoStorage
from aiohttp_session import setup, get_session
from aiohttp_security import check_permission, \
    is_anonymous, remember, forget, \
    setup as setup_security, SessionIdentityPolicy
import asyncio
from dotmap import DotMap
from modules.dbMongo import DbMongo
from modules.jsonValidator import JsonValidator
from modules.authPolicy import AuthorizationPolicy
import os
import sys

# Импортируем routes для api
from routes import setup_routes
# Испортируем логер и конфигурации сервера
from settings import config, logger

async def init_app():
    """Формируем настроки для сервера."""
    try:
        conf = DotMap(config)

        application = web.Application()
        application['config'] = conf
        application['config']['validator'] = JsonValidator({'schemePath': os.path.join(os.getcwd(), 'schema')})

        if conf.db.type == 'mongodb':
            db = DbMongo(conf.db.host, conf.db.port)
            application['config']['db'] = db

            '''
            session_collection = db.database['sessions']
            max_age = 3600 * 24 * 365
            '''

            policy = SessionIdentityPolicy()

            setup(
                application,
                MongoStorage(db.database['sessions'])
                )
            
            setup_security(
                application, 
                policy, 
                AuthorizationPolicy(db)
                )

        setup_routes(application, logger)
        return application

    except Exception as e:
        logger.error('Error create application.')
        logger.error(e)
        return False

if __name__ == '__main__':
    if sys.version_info.major < 3 or sys.version_info.minor < 8:
        logger.error('Error, min need python 3.8')
        sys.exit(1)

    try:
        loop = asyncio.get_event_loop()
        logger.info('Init app...')
        app = loop.run_until_complete(init_app())
        if not app:
            raise IOError("Errors run unit init app.")
        else:
            web.run_app(app, 
                host=config['host'], 
                port=config['port']
                )

    except Exception as ex:
        logger.error('Error run application.')
            

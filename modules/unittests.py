import unittest
import asyncio
from dbMongo import DbMongo
import logging

logger = logging.getLogger("tester")

class TestStringMethods(unittest.TestCase):
        def __init__(self):
            self.collection = 'test'
            self.element = {"name": 'test', "description": 'test', "userGroup": 'usergroup', "info": 'test1'}
            self.updateElement = {"name": 'test1', "description": 'test1', "userGroup": 'usergroup', "info": 'test1'}
            self.testDb = DbMongo('localhost', 27017)

        def addElementInCollection(self):
            async def testMain():
                project = await self.testDb.addElementInCollection(self.collection, self.element)
                return project
            
            self.assertEqual(asyncio.run(testMain()), True)

        def getElementInCollection(self):
            async def testMain(res):
                if res:
                    project = await self.testDb.getElementInCollection(self.collection, list(self.element.keys())[0], self.element[list(self.element.keys())[0]])
                else:
                    project = await self.testDb.getElementInCollection(self.collection, 'fail', 'fail')
                return project
            
            self.assertEqual(asyncio.run(testMain(True)), self.element)
            self.assertEqual(asyncio.run(testMain(False)), False)

        def checkElementInCollection(self):
            async def testMain():
                project = await self.testDb.checkElementInCollection(self.collection, list(self.element.keys())[0], self.element[list(self.element.keys())[0]])
                return project
            
            self.assertEqual(asyncio.run(testMain()), True)

        def getShortElementsInCollection(self):
            async def testMain():
                project = await self.testDb.getShortElementsInCollection(self.collection)
                return project
            
            self.assertEqual(asyncio.run(testMain()), [{self.element}])

        def getLongElementsInCollection(self):
            async def testMain():
                project = await self.testDb.getLongElementsInCollection(self.collection)
                return project
            
            self.assertEqual(asyncio.run(testMain()), [{self.element}])

        def updateElementsInCollection(self):
            async def testMain():
                project = await self.testDb.updateElementsInCollection(self.collection, list(self.element.keys())[0], self.element[list(self.element.keys())[0]], self.updateElement)
                return project
            
            self.assertEqual(asyncio.run(testMain()), True)

        def deleteElementsInCollection(self):
            async def testMain():
                project = await self.testDb.deleteElementsInCollection(self.collection, list(self.element.keys())[0], self.element[list(self.element.keys())[0]])
                return project
            
            self.assertEqual(asyncio.run(testMain()), self.element)

if __name__ == '__main__':
    unittest.main()
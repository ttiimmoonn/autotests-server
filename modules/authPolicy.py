
import motor.motor_asyncio
from aiohttp_security import check_permission, \
    is_anonymous, remember, forget, \
    setup as setup_security, SessionIdentityPolicy
from aiohttp_security.abc import AbstractAuthorizationPolicy
from passlib.hash import pbkdf2_sha256
import logging

logger = logging.getLogger("tester")

class AuthorizationPolicy(AbstractAuthorizationPolicy):
    def __init__(self, dbengine):
        """ Авторизация пользователя
        
        Коллекция users:
            _id
            login
            passwd
            is_superuser
            disabled

        Коллекция permissions:
            _id
            user_id
            perm_name
        """
        self.dbengine = dbengine
        self.users_collection = 'users'
        self.permissions_collection = 'permissions'

    async def authorized_userid(self, identity):
        if identity is None:
            return False

        try: 
            if await self.dbengine.checkElementInCollection(self.users_collection, 'login', identity):
                return True
            else:
                return None
        except Exception as e:
            logger.debug(f'{e}')
            return False



    async def permits(self, identity, permission, context=None):
        if identity is None:
            return False
        
        try:
            if await self.dbengine.checkElementInCollection(self.users_collection, 'login', identity):
                user = await self.dbengine.getElementInCollection(self.users_collection, 'login', identity)
                if user:
                    if user.get('is_superuser', False):
                        return True
                    
                    user_in_permission = await self.dbengine.getLongElementsInCollection(self.permissions_collection, filterDb={'_id': user.get('_id')})

                    if user_in_permission:
                        for perm in user_in_permission:
                            if perm.get('perm_name') == permission:
                                return True
                        return False
                    else:
                        return False
            else:
                return None
        except Exception as e:
            logger.debug(f'{e}')
            return False

async def check_credentials(db_engine, username, password):
    if await db_engine.checkElementInCollection('users', 'login', username):
        user = await db_engine.getElementInCollection('users', 'login', username)
        if user:
            hashed = user.get('passwd', '')
            return pbkdf2_sha256.verify(password, hashed)
    else:
        return False
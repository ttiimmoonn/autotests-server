import motor.motor_asyncio
from bson import ObjectId
import logging
import asyncio

logger = logging.getLogger("tester")

class DbMongo:    
    def __init__(self, host, port, *args, **kwargs):
        self.database = motor.motor_asyncio.AsyncIOMotorClient(host, port).autotests

        self.length = kwargs.get('length', 100000)

    async def checkElementInCollection(self, collection, keyProps='', valueProps='', filterDb={}):
        """Проверка элемента в коллекции через параметр key с значением valueProps. 
        
        Возвращает True если элемент есть в колекции и False если его там нет.
        
        Keyword arguments:
        collection -- имя коллекции (required)
        keyProps  -- название параметра элемента для проверки, type = String (required)
        valueProps  -- значение параметра элемента для проверки, type = String (required)
        filterDb -- фильтр для поиска в коллекции
        """
        try:
            if keyProps:
                filterDb[keyProps] = valueProps
                
            if filterDb.get('_id', False):
                filterDb['_id'] = ObjectId(filterDb.get('_id'))

            elements = []
            elementsDb = self.database[collection].find(filterDb)

            for element in await elementsDb.to_list(length=self.length):
                elements.append(element.get(keyProps, ''))
            if valueProps in elements:
                return True
        except Exception as e:
            logger.error(f'Error check element in collection "{collection}" via "{keyProps}"...')
            logger.error(f'\tCollection:{collection}"; keyProps: "{keyProps}"; valueProps: "{valueProps}"; filterDb: "{filterDb}".')
            logger.debug(e)
            return False
        return False

    async def getShortElementsInCollection(self, collection, filterDb={}, *args, **kwargs):
        """Получить все элементы коллекции с коротким набором параметров.
        
        Возвращает список всех элементов данной коллекции с базовым набором параметров:
        _id - id
        name - имя
        description - описание элемента
        userGroup - группа пользователей
        
        Keyword arguments:
        collection -- имя коллекции (required)
        filterDb -- фильтр для поиска в базе
        otherProps -- список дополнительных параметров которые необходимо забрать
        """

        otherProps = kwargs.get('otherProps', [])

        try:
            if filterDb.get('_id', False):
                filterDb['_id'] = ObjectId(filterDb.get('_id'))

            elements = []
            elementsDb = self.database[collection].find(filterDb)

            for element in await elementsDb.to_list(length=self.length):
                newElement = {
                    '_id': str(element['_id']),
                    'name': element['name'],
                    'description': element['description'],
                    'userGroup': element.get('userGroup', '')
                }

                if otherProps:
                    for props in otherProps:
                        newElement[props] = element.get(props, '')

                elements.append(newElement)
                
        except Exception as e:
            logger.error(f'Error get elements in collection "{collection}"...')
            logger.debug(e)
            return False
        return elements

    async def getLongElementsInCollection(self, collection, filterDb={}, *args, **kwargs):
        """Получить все элементы коллекции с коротким набором параметров.
        
        Возвращает список всех элементов данной коллекции с базовым набором параметров:
        _id - id
        name - имя
        description - описание элемента
        userGroup - группа пользователей
        
        Keyword arguments:
        collection -- имя коллекции (required)
        filterDb -- фильтр для поиска в базе
        """

        try:
            if filterDb.get('_id', False):
                filterDb['_id'] = ObjectId(filterDb.get('_id'))

            elements = []
            elementsDb = self.database[collection].find(filterDb)

            for element in await elementsDb.to_list(length=self.length):
                element['_id'] = str(element['_id'])
                elements.append(element)
                
        except Exception as e:
            logger.error(f'Error get elements in collection "{collection}"...')
            logger.debug(e)
            return False
        return elements

    async def getElementInCollection(self, collection, keyProps='', valueProps='', filterDb={}, *args, **kwargs):
        """Получить элемент коллекции с полным набором параметров.
        
        Возвращает словарь элемента данной коллекции с полным набором параметров:
        _id - id
        name - имя
        description - описание элемента
        userGroup - группа пользователей
        
        Keyword arguments:
        collection -- имя коллекции (required)
        keyProps  -- название параметра элемента для проверки, type = String (required)
        valueProps  -- значение параметра элемента для проверки, type = String (required)
        """

        try:
            if keyProps:
                filterDb[keyProps] = valueProps
            if '_id' in filterDb.keys():
                filterDb['_id'] = ObjectId(filterDb['_id'])

            element = await self.database[collection].find_one(filterDb)
            element['_id'] = str(element['_id'])

        except Exception as e:
            logger.error(f'Error get element in collection "{collection}"...')
            logger.debug(f'Collection: "{collection}"; keyProps: "{keyProps}"; valueProps: "{valueProps}".')
            logger.debug(e)
            return False
        if element:
            return element
        return False

    async def addElementInCollection(self, collection, element, *args, **kwargs):
        """Добавить элемент в коллекцию.
        
        Возвращает True если элемент был добавлен успешно и False, если нет.
        
        Keyword arguments:
        collection -- имя коллекции (required)
        element -- фильтр для поиска в базе
        """

        try:
            result = await self.database[collection].insert_one(element)
        except Exception as e:
            logger.error(f'Error add element in collection "{collection}"...')
            logger.debug(f'Collection: "{collection}"; element: "{element}".')
            logger.debug(e)
            return False
        return True

    async def updateElementsInCollection(self, collection, keyProps, valueProps, element, filterDb={}, *args, **kwargs):
        """ Обновить элемент в коллекции.
        
        Возвращает True если элемент был обновлен успешно и False, если нет.
        
        Keyword arguments:
        collection -- имя коллекции (required)
        keyProps -- ключ по которому будет найден элемент (required)
        valueProps -- значение по которому будет найден элемент (required)
        element -- параметры которые необходимо обновить (required)
        """

        try:
            filterDb[keyProps] = valueProps
            if '_id' in filterDb.keys():
                filterDb['_id'] = ObjectId(filterDb['_id'])

            oldElementDb = await self.getElementInCollection(collection, keyProps, valueProps, filterDb=filterDb)

            if not oldElementDb:
                return False

            idElement = ObjectId(oldElementDb['_id'])
            oldElementDb.update(element)
            del oldElementDb['_id']

            result = await self.database[collection].replace_one({'_id': idElement}, oldElementDb)

        except Exception as e:
            logger.error(f'Error update element in collection "{collection}"...')
            logger.debug(f'Collection: "{collection}"; new element: "{oldElementDb}"; update dict: "{element}"; keyProps: "{keyProps}"; valueProps: "{valueProps}"')
            logger.debug(e)
            return False
        return True

    async def deleteElementsInCollection(self, collection, keyProps='', valueProps='', filterDb={}, *args, **kwargs):
        """Удалить элемент из коллекции.
        
        Возвращает True если элемент был удален успешно и False, если нет.
        
        Keyword arguments:
        collection -- имя коллекции (required)
        keyProps -- ключ для параметра для удаления
        valueProps -- значение для параметра для удаления
        """

        try:
            if keyProps:
                filterDb[keyProps] = valueProps
            if '_id' in filterDb.keys():
                filterDb['_id'] = ObjectId(filterDb['_id'])
            
            result = await self.database[collection].delete_many(filterDb)
        except Exception as e:
            logger.error(f'Error add element in collection "{collection}"...')
            logger.debug(f'Collection: "{collection}"; keyProps: "{keyProps}"; valueProps: "{valueProps}".')
            logger.debug(e)
            return False
        return True


    def databaseCheck(self):
        """Проверка возможности записи и чтения в базу данных.
        """
        pass


'''
async def main():
    element1 = {"name": 'test1', "description": 'test', "userGroup": 'usergroup', "info": 'test1'}


    test = DbMongo('localhost', 27017)

    project = await test.addElementInCollection('project', element1.copy())

    project = await test.getLongElementsInCollection('project')
    print(project)

    project = await test.getShortElementsInCollection('project')
    print(project)

    project = await test.getElementInCollection('project', 'name', 'test1')
    print(project)

    project = await test.updateElementsInCollection('project', 'name', 'test1', {'description': 'newTest', "userGroup": 'newUsergroup',})
    print(project)

    project = await test.getElementInCollection('project', 'name', 'test1')
    print(project)

    project = await test.deleteElementsInCollection('project', 'info', 'test1')
    print(project)

    project = await test.getLongElementsInCollection('project')
    print(project)


asyncio.run(main())

'''
from jsonschema import Draft6Validator
import os
import json
import pprint
import logging

logger = logging.getLogger("tester")

class JsonValidator:
  def __init__(self, prop):
      self.schemePath = prop.get('schemePath', '')
      self.schemes = dict()
      
      self.getScheme()

  def readsScheme(self, path):
    try:
      with open(path) as json_file:
          date = json.load(json_file)
      return date
    except Exception as e:
      logger.debug(f'Fail read file "{path}"...')
      logger.error(e)
      return dict()

  def getScheme(self):
    logger.debug('Get validate scheme...')
    self.schemes = {
      x.split('.')[:1][0]: {
        'path': os.path.join(self.schemePath, x), 
        'scheme': self.readsScheme(os.path.join(self.schemePath, x))} for x in os.listdir(self.schemePath)}

  def checkRequest(self, method, body):
    logger.debug(f'Check method "{method}"...')
    if method in self.schemes.keys():
      try:
        Draft6Validator(self.schemes[method]['scheme']).validate(body)
        logger.debug(f'Succ validator...')
        return {'state': True}
      except Exception as e:
        logger.debug(f'Error in validate...')
        logger.error(e)
        return {'state': False, 'error': f'{e}'}
    else:
      logger.debug(f'Method not found in method keys...')
      return {'state': False, 'error': f'Method "{method}" is not found...'}
